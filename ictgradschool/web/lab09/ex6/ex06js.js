/**
 * Created by qpen546 on 5/04/2017.
 */
var pages = document.getElementsByClassName("page");
var pageNum = 0;

function flip() {
    if (pageNum<pages.length) {
        pages[pageNum].style.webkitAnimation = "flippage 1.5s forwards";
        pages[pageNum].style.webkitAnimationDelay = "0s";
        pages[pageNum].style.transformOrigin = "top left";
        pages[pageNum].addEventListener("webkitAnimationEnd", pages[pageNum].style.zIndex = pageNum);
        pageNum++;
    }
}

