var customers = [
    {"name": "Peter Jackson", "gender": "male", "year_born": 1961, "joined": "1997", "num_hires": 17000},

    {"name": "Jane Campion", "gender": "female", "year_born": 1954, "joined": "1980", "num_hires": 30000},

    {"name": "Roger Donaldson", "gender": "male", "year_born": 1945, "joined": "1980", "num_hires": 12000},

    {"name": "Temuera Morrison", "gender": "male", "year_born": 1960, "joined": "1995", "num_hires": 15500},

    {"name": "Russell Crowe", "gender": "male", "year_born": 1964, "joined": "1990", "num_hires": 10000},

    {"name": "Lucy Lawless", "gender": "female", "year_born": 1968, "joined": "1995", "num_hires": 5000},

    {"name": "Michael Hurst", "gender": "male", "year_born": 1957, "joined": "2000", "num_hires": 15000},

    {"name": "Andrew Niccol", "gender": "male", "year_born": 1964, "joined": "1997", "num_hires": 3500},

    {"name": "Kiri Te Kanawa", "gender": "female", "year_born": 1944, "joined": "1997", "num_hires": 500},

    {"name": "Lorde", "gender": "female", "year_born": 1996, "joined": "2010", "num_hires": 1000},

    {"name": "Scribe", "gender": "male", "year_born": 1979, "joined": "2000", "num_hires": 5000},

    {"name": "Kimbra", "gender": "female", "year_born": 1990, "joined": "2005", "num_hires": 7000},

    {"name": "Neil Finn", "gender": "male", "year_born": 1958, "joined": "1985", "num_hires": 6000},

    {"name": "Anika Moa", "gender": "female", "year_born": 1980, "joined": "2000", "num_hires": 700},

    {"name": "Bic Runga", "gender": "female", "year_born": 1976, "joined": "1995", "num_hires": 5000},

    {"name": "Ernest Rutherford", "gender": "male", "year_born": 1871, "joined": "1930", "num_hires": 4200},

    {"name": "Kate Sheppard", "gender": "female", "year_born": 1847, "joined": "1930", "num_hires": 1000},

    {"name": "Apirana Turupa Ngata", "gender": "male", "year_born": 1874, "joined": "1920", "num_hires": 3500},

    {"name": "Edmund Hillary", "gender": "male", "year_born": 1919, "joined": "1955", "num_hires": 10000},

    {"name": "Katherine Mansfield", "gender": "female", "year_born": 1888, "joined": "1920", "num_hires": 2000},

    {"name": "Margaret Mahy", "gender": "female", "year_born": 1936, "joined": "1985", "num_hires": 5000},

    {"name": "John Key", "gender": "male", "year_born": 1961, "joined": "1990", "num_hires": 20000},

    {"name": "Sonny Bill Williams", "gender": "male", "year_born": 1985, "joined": "1995", "num_hires": 15000},

    {"name": "Dan Carter", "gender": "male", "year_born": 1982, "joined": "1990", "num_hires": 20000},

    {"name": "Bernice Mene", "gender": "female", "year_born": 1975, "joined": "1990", "num_hires": 30000}
];

var table = document.getElementById("contents");
var maleNum = 0;
var femalesNum = 0;
var peopleNum1 = 0;
var peopleNum2 = 0;
var peopleNum3 = 0;
var loyaltyStatus;
var goldNum = 0;
var sliverNum = 0;
var bronzeNum = 0;

document.getElementsByTagName("table")[0].style.border = "1px solid black";
document.getElementsByTagName("table")[0].style.margin = "auto";
for (var i = 0; i < customers.length; i++) {
    var rowData = customers[i];
    var row = table.insertRow();

    for (var key in rowData) {
        var cellData = rowData[key];
        var cell = row.insertCell();
        cell.style.border = "1px solid black";
        if (i == 0) {
            cell.appendChild(document.createTextNode(key.split('_').join(' ')));
        } else {
            cell.appendChild(document.createTextNode(cellData));
        }
    }

    switch (rowData.gender) {
        case "male":
            maleNum++;
            break;
        case "female":
            femalesNum++;
            break;
    }


    var age = 2017 - rowData.year_born;
    if (age <= 30) {
        peopleNum1++;
    } else if (age <= 64) {
        peopleNum2++;
    } else {
        peopleNum3++;
    }


    var loyaltyCell = row.insertCell();
    loyaltyCell.style.border = "1px solid black";

    if (i == 0) {
        loyaltyCell.appendChild(document.createTextNode("loyalty Status"))
    } else {
        var frequency = rowData.num_hires / ((2017 - rowData.joined) * 52);
        if (frequency < 1) {
            loyaltyStatus = "bronze";
            bronzeNum++;
        } else if (frequency <= 4) {
            loyaltyStatus = "silver";
            sliverNum++;
        } else {
            loyaltyStatus = "gold";
            goldNum++;
        }
        loyaltyCell.appendChild(document.createTextNode(loyaltyStatus));
    }
}

var summaryData = {
    "total_male": maleNum,
    "total_female": femalesNum,
    "total_people_between_0_and_30": peopleNum1,
    "total_people_between_31_and_64": peopleNum2,
    "total_people_geater_than_65": peopleNum3,
    "total_gold_membership": goldNum,
    "total_sliver_membership": sliverNum,
    "total_bronze_membership": bronzeNum
};
var summary = document.getElementById("summary");
document.getElementsByTagName("table")[1].style.border = "1px solid black";
document.getElementsByTagName("table")[1].style.margin = "5% auto";


for (var i = 0; i < 2; i++) {
    var row = summary.insertRow();
    for (var key in summaryData) {
        var cell = row.insertCell();
        cell.style.border = "1px solid black";
        if (i == 0) {
            cell.appendChild(document.createTextNode(key.split('_').join(' ')))
        } else {
            cell.appendChild(document.createTextNode(summaryData[key]))
        }
    }

}